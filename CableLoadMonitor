#!/bin/sh

#
# DOCSIS cable load monitor 
#
# tool to monitor downstream load on DOCSIS cable networks
#
# https://codeberg.org/sparkie/docsis-cable-load-monitor
#

echo "$@" | egrep -q -e '-g|-h( [^0-9]|[^ ]|$)' || {
exec 8> /tmp/CableLoadMonitor.lck
flock -w0 8 || { echo another instance is already active; exit 255; }
}

cat > $0_$$.awk << !

func setup( \
    )
{
    CLARGS = "$0|$1|$2|$3|$4|$5|$6|$7|$8|$9|${10}|${11}|${12}|${13}|${14}|${15}|${16}|${17}|${18}|${19}|${20}"

    # avoid dangling include files. incore inode still exists
    system("rm $0_$$.awk")

    # take care of shell expansion (if security is an issue)
    setup_lib($#, "awk|" CLARGS, $$, "`pwd`", "`cat /etc/hostname`", "$R_")
    PRP(CLARGS "\n")

    # required if Sundtek hardware is in use as we install with '-service' option
    LD_PL = "LD_PRELOAD=/opt/lib/libmediaclient.so "

    # where to find your TC4400 cable modem (if any)
    CABLE_MODEM_IP = "192.168.100.1"
    CABLE_MODEM_USER = "admin"
    CABLE_MODEM_PASS = "'bEn2o#US9s'"

    # file current recording parameters as incompatible changes must trigger a new RRD generation
    CONF_FILE = INSTPATH "CableLoadMonitor.cfg"
    FREQ_FACTOR = 1000000

    # default RRDBASE
    RRDBASE_FILE = INSTPATH "CableLoadMonitor.rrd"
    RRDBASE_RRASTEPS = 10                       # defaults to 10s
    RRDBASE_HISTSIZE = 60 * 60 * 24 * 7         # defaults to 1 week

    # default RRDGRAPH
    RRDGRAPH_EXT = ".png"
    RRDGRAPH_WIDTH = 1400
    RRDGRAPH_HEIGHT = 1000
    RRDGRAPH_Y_UPPER_LIMIT = 40                 # scale Y axis to this minimum
    RRDGRAPH_UPDATE_RATE = 300                  # lower the graph update rate for higher hist sizes
#    RRDGRAPH_NAME = set elsewhere
#    RRDGRAPH_TMP = set elsewhere
#    BWIDTH_SNAPSHOT = set elsewhere

    # feel free to add other history sizes (aka generated graphs) as well
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60            " |1h"
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60 * 6        " |6h"
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60 * 24       " |1d"
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60 * 24 * 7   " |7d"
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60 * 24 * 30  " |30d"
    RRDGRAPHS[RRDGRAPHS_CNT++] = 60 * 60 * 24 * 365 " |365d"

    # ---
    rrdbase_rrasteps = RRDBASE_RRASTEPS 
    rrdbase_histsize = RRDBASE_HISTSIZE   
    rrdgraph_width = RRDGRAPH_WIDTH
    rrdgraph_height = RRDGRAPH_HEIGHT

    # --- define this to display a black theme
    if (++use_black_theme) {
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"ARROW#ffffff\""   " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"BACK#000000\""    " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"CANVAS#000000\""  " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"GRID#ffffff\""    " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"MGRID#ffffff\""   " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"FONT#ffffff\""    " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"AXIS#ffffff\""    " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--color \"FRAME#ffffff\""   " \\\\\n"
        IF_BLACK_THEME = IF_BLACK_THEME "--border 1"                 " \\\\\n"
    }
    # --- define this to rescale the sum graph (experimental)
    if (use_2nd_yaxis) {
        IF_2ND_YAXIS_SCALE = 10

        IF_2ND_YAXIS = IF_2ND_YAXIS "--right-axis " IF_2ND_YAXIS_SCALE ":0"        " \\\\\n"
        IF_2ND_YAXIS = IF_2ND_YAXIS "--right-axis-label sum"                       " \\\\\n"

        SUM_DEF = SUM_DEF sprintf("DEF:sum_=" RRDBASE_FILE ":sum:AVERAGE", i, i)   " \\\\\n"
        SUM_DEF = SUM_DEF "CDEF:sum=sum_," IF_2ND_YAXIS_SCALE ",/"                 " \\\\\n"
    } else {
        SUM_DEF = SUM_DEF sprintf("DEF:sum_=" RRDBASE_FILE ":sum:AVERAGE", i, i)   " \\\\\n"
        SUM_DEF = SUM_DEF "CDEF:sum=sum_"                                          " \\\\\n"
    }
}

func read_cfg( \
    line, CMD, a)
{
    if (ex("ls " CONF_FILE, 0, "^(" CONF_FILE ")$")) {
        CMD = "cat " CONF_FILE
        #PR("reading last recently used downstream channel frequencies and RRD parameters")
        FREQ_SPEC_CFG = ex_line(CMD)
        while ((line = ex_line(CMD)) != -1) {
            if (match(line, "^RRA:AVERAGE:.*:([0-9]+):([0-9]+)$", a)) {
                RRA_STEPS_CFG = a[1]
                HIST_SIZE_CFG = a[2] * RRA_STEPS_CFG
            }
        }
    }
}

func exit_(msg \
    )
{
    PRE(msg)
    PRE("exiting...")
    ++ERR; exit
}

func set_freqs(str, \
    i, a)
{
    if (!match(str, "^[0-9]+(:[0-9]+)*$")) {
        return 0
    }
    split(str, a, ":")
    for (i = 1; a[i]; ++i) {
        FREQ[FREQ_CNT++] = a[i]
    }
    return 1
}

func retrieve_down_freqs(freqs, \
    URL, ignore, line, WGET, LYNX, i, a)
{
    URL = "http://" CABLE_MODEM_IP "/cmconnectionstatus.html"

    if (freqs == "force_new_scan" || !freqs) {

        #
        # specific to TC4400 yet
        #
        PR("trying to retrieve downstream channel frequencies from TC4400 modem")
        WGET = "echo; wget --connect-timeout=1 -t 1 -q " \
                          "--http-user=" CABLE_MODEM_USER " " \
                          "--http-password=" CABLE_MODEM_PASS " " URL " -O -"
        LYNX = "lynx -nolist -width 300 -dump -stdin"
        while ((line = ex_line(WGET)) != -1) {
            if (match(line, "<script")) {
                ++ignore
            } else if (ignore) {
                if (match(line, "/script?")) {
                    ignore = 0
                }
            } else {
                print line |& LYNX
            }
        }
        close(LYNX, "to")
        while (LYNX |& getline line > 0) {
            if (match(line, "Locked +SC-QAM +Downstream +Bonded +([0-9]+) ", a)) {
                FREQ[FREQ_CNT++] = a[1] / FREQ_FACTOR
            }
        }
        close(LYNX)
    } else if (freqs) {
        PR("evaluating given downstream channel frequencies")
        if (!set_freqs(freqs)) {
            exit_("can't handle the given downstream channel frequency specification")
        }
    } else {
        # error -> will exit
    }
    PRF("downstream channel frequencies now in use: ")
    if (!FREQ_CNT) {
        exit_("could not retrieve any")
    }
    PRF("[ " FREQ_CNT " ] ")
    for (i = 0; i < FREQ_CNT; ++i) {
        PRF(FREQ[i] " ")
        BWIDTH_OF[i] = "na"
        FREQ_SPEC_NEW = FREQ_SPEC_NEW (FREQ_SPEC_NEW ? ":" : "") FREQ[i]
    }
    PR("")
}

func assemble_rrdcreate_cmd( \
    CMD, i)
{
    CMD = "rrdtool create " RRDBASE_FILE " -s 1"           " \\\\\n"
    for (i = 0; i < FREQ_CNT; ++i) {
        CMD = CMD sprintf("DS:f%02d:GAUGE:120:U:U", i)     " \\\\\n"
    }
    CMD = CMD sprintf("DS:sum:GAUGE:120:U:U")              " \\\\\n"
    CMD = CMD sprintf("RRA:AVERAGE:0.1:" rrdbase_rrasteps ":" int(rrdbase_histsize / rrdbase_rrasteps))
    RRDB_CCMD_NEW = CMD
}

func assemble_rrdgraph_cmd(end, size, postfx, \
    group, GROUPS, CMD, i, k)
{
    # use the first version if interested in average and min too
    GROUPS = "MAX:max:MIN:min:LAST:last:AVERAGE:avg"
    GROUPS = "MAX:max:LAST:last"
    split(GROUPS, group, ":")

    CMD = "rrdtool graph " RRDGRAPH_TMP postfx " -a PNG -l 0 -u " RRDGRAPH_Y_UPPER_LIMIT * FREQ_FACTOR        " \\\\\n" \
          "--title=\"Graph generated at \`date\`\""                                                    " \\\\\n" \
          "-w " rrdgraph_width " -h " rrdgraph_height                                                  " \\\\\n" \
          "--end " end " --start end-" size "s"                                                        " \\\\\n" 
    CMD = CMD                                                                                          " \\\\\n"
    CMD = CMD IF_BLACK_THEME                                                                           " \\\\\n"
    CMD = CMD IF_2ND_YAXIS                                                                             " \\\\\n"
    CMD = CMD                                                                                          " \\\\\n"
    for (i = 0; i < FREQ_CNT; ++i) {
        CMD = CMD sprintf("DEF:f%02d=" RRDBASE_FILE ":f%02d:AVERAGE", i, i)                            " \\\\\n" 
        if (i) {
            CMD = CMD sprintf("CDEF:f%02d_=f%02d_,f%02d,+", i, i - 1, i)                               " \\\\\n" 
        } else {
            CMD = CMD sprintf("CDEF:f%02d_=0,f%02d,+", i, i)                                           " \\\\\n" 
        }
    }
    CMD = CMD SUM_DEF                                                                                  " \\\\\n" 
    CMD = CMD                                                                                          " \\\\\n"
    if (display_mode == 1) {
        for (i = 0; i < FREQ_CNT; ++i) {
            CMD = CMD sprintf("\"LINE:f%02d_#ff0000:%d\"", i, FREQ[i])                                 " \\\\\n" 
        }
    } else {
        for (i = 0; i < FREQ_CNT; ++i) {
            if (i < FREQ_CNT / 2) {
                CMD = CMD sprintf("\"LINE:f%02d#ff0000:%d\"", i, FREQ[i])                              " \\\\\n" 
            } else {
                CMD = CMD sprintf("\"LINE:f%02d#0000ff:%d\"", i, FREQ[i])                              " \\\\\n" 
            }
        }
    }
    CMD = CMD                                                                                          " \\\\\n"
    CMD = CMD "\"LINE:sum#00ff00:sum\\\l\""                                                            " \\\\\n"
    CMD = CMD                                                                                          " \\\\\n"
    for (k = 1; group[k]; k += 2) {
        for (i = 0; i < FREQ_CNT; ++i) {
            CMD = CMD sprintf("\"GPRINT:f%02d:%s:%%3.0lf %%s\"", i, group[k])                          " \\\\\n"
        }
        CMD = CMD sprintf("\"GPRINT:%s:%s:%%3.0lf %%s\"", "sum_", group[k])
        CMD = CMD "\"    " group[k + 1] "\\\l\""                                                       " \\\\\n"
    }
    return CMD
}

func assemble_rrdgraphs_cmd( \
    i, a)
{
    for (i = 0; i < RRDGRAPHS_CNT; ++i) {
        split(RRDGRAPHS[i], a, "|")
        if (a[1] > rrdbase_histsize) {
            break
        }
        RRDGRAPH_CMD[i] = assemble_rrdgraph_cmd("now", gensub(" +", "", "g", a[1]))
        RRDGRAPH_FILE[i] = RRDGRAPH_NAME "_" a[2] RRDGRAPH_EXT
        PR("generating graph for: " sprintf("%4s", a[2]) " recording length, size " rrdgraph_width "x" rrdgraph_height " pixels")
    }
}

func scan_down_channels( \
    tuner, tuners, retry, resume, passc, bwidth, RRDUPD_STR, BWIDTH_SUM, a, i, k, kk, line)
{
    PRF(strftime("%T: "))
    PRP("--------------------------------------------------------------------------------")

    if (sequential) {
        tuners = 1    
    } else {
        tuners = tuners_found
    }
    tuner = 0
    retry = 0
    for (i = 0; i < FREQ_CNT; i += tuners) {
        if (!retry) {
            resume = 0
        }
        for (k = resume; k < tuners; ++k) {
            if (i + k == FREQ_CNT) break

            #
            # tuning via mediaclient not in use ATM
            #   ex("timeout 10 /opt/bin/mediaclient -d /dev/dvb/adapter0/frontend0 -m DVBC -f " FREQ[i] * FREQ_FACTOR " -M Q256 -S 6952000")
            #
            # in case of very small bandwidth values limit the measurement 
            # interval to something useful to keep the overall scan time sufficiently low.
            #
            # the arbitrarily chosen stop value of 80000 appears to be high enough to even 
            # handle high bandwidths with enough accuracy.
            #
            CMD = "{ " LD_PL "timeout 10 dvbtune -c " tuner " -f " FREQ[i + k] * FREQ_FACTOR " -s 6952 -qam 256; " \
                       LD_PL "timeout 1 dvbsnoop -adapter " tuner " -s bandwidth 8190 -n 80000" "; } 2>&1" 
            tuner = ++tuner % tuners_found
            PRP(CMD)
            print CMD |& SH[k]
            close(SH[k], "to")
        }
        for (k = resume; k < tuners; ++k) {
            if (i + k == FREQ_CNT) break
            passc = 0
            while (SH[k] |& getline line > 0) {
                if (verbose) {
                    PRP(line)
                }
                if (match(line, "Bit error rate: ([0-9]+)$", a) \
                 && (a[1] + 0) \
                 && !dvbtune_ignerrs) {
                    PR("\n\n" a[0] ", please check your cables or use '-i' option\n")
                    break
                } else if (match(line, "^FE_STATUS: FE_HAS_SIGNAL FE_HAS_LOCK FE_HAS_CARRIER FE_HAS_VITERBI FE_HAS_SYNC")) {
                    passc += 2
                } else if (match(line, "^## PID:.*Avrg: +([^ ]+) ", a)) {
                    PRFP(line "   ")
                    bwidth = a[1]
                    passc += 3
                }
            }
            close(SH[k])

## simulate tuner failure
#if (++CNT == 2) {
##    PR(" <== TRIGGERED FAIL ON [" CNT "]")
#    passc = 0
#}
            if (passc != 7) {

                #
                # UPDATE as of 2019_08_03:
                #  - in the event of dvbsnoop failure: no longer exit but try to recover instead
                #  - you may want to reload your DVB drivers in a proper way to overcome the failure
                #
                PRF("\ndvbtune/dvbsnoop fails for [ " FREQ[i + k] " ] [ 7 != " passc " ]")
                if (failreboot) {
                    PR(", rebooting...")
                    PRF("waiting for 30s grace period to expire: ")
                    for (kk = 0; kk < 30; ++kk) {
                        ex("sleep 1")
                        PRF(".")
                    }
                    PR("")
                    ex("sudo /sbin/init 6", 1)
                }
                PR(", retrying...")

                #
                # place your specific driver reload code here
                #
                ex("systemctl stop sundtek; sleep 5; systemctl start sundtek; sleep " 5 + 10 * tuners_found, 1)

                # recoil looping params for resume
                for (kk = k + 1; kk < tuners; ++kk) {
                    if (i + kk == FREQ_CNT) break
                    close(SH[kk])
                }
                i -= tuners
                resume = k
                ++retry
                break
#                exit_("dvbsnoop fails")
            }
            BWIDTH_OF[i + k] = int(bwidth + 0.5)
            PRF(sprintf("%6s", BWIDTH_OF[i + k]))
            PRP("")
            BWIDTH_SUM = 0
            RRDUPD_STR = "rrdtool update " RRDBASE_FILE " N"
            for (kk = 0; kk < FREQ_CNT; ++kk) {
                BWIDTH_SUM += BWIDTH_OF[kk]
                RRDUPD_STR = RRDUPD_STR ":" BWIDTH_OF[kk] * 1000
            }
            #
            # do not update until the initial scan completes
            #
            if (BWIDTH_OF[FREQ_CNT - 1] != "na") {
                RRDUPD_STR = RRDUPD_STR ":" BWIDTH_SUM * 1000
                ex(RRDUPD_STR, 2)
            } 
        }
        if (k == tuners) {
            retry = 0
        }
    }
    PR(sprintf("%7s", BWIDTH_SUM))
    print BWIDTH_SUM " kbit/s" > BWIDTH_SNAPSHOT; close(BWIDTH_SNAPSHOT)
}

func generate_rrdgraphs( \
    i, a)
{
    for (i = 0; RRDGRAPH_CMD[i]; ++i) {
        if (i && RRDGRAPH_LUPD[i - 1] - RRDGRAPH_LUPD[i] < RRDGRAPH_UPDATE_RATE) {
            break
        }
        if (ex(RRDGRAPH_CMD[i], 256, "^(.*ERROR.*)$", a)) {
            PR(a[1])
            exit_("rrdtool graph fails")
        }
        ex("mv " RRDGRAPH_TMP " " RRDGRAPH_FILE[i], 2)
        RRDGRAPH_LUPD[i] = strftime("%s")
    }
}

func watch_the_scenery( \
    last, retry, RETRIES, i)
{
    RETRIES = 30

    #
    # some inital setup code specific to certain devices. this may fail 
    # on one or another device. don't care.
    #

    #
    # fix for:
    # * Silicon Labs Si2168 card not compatible? · Issue #1 · sp4rkie/docsis-cable-load-monitor
    # https://codeberg.org/sparkie/docsis-cable-load-monitor/issues/1
    #
    ex("echo 0 | sudo tee -a /sys/module/dvb_core/parameters/dvb_powerdown_on_sleep", 0)

    if (!tuners_cfg) ++tuners_cfg
    retry = RETRIES
    while (1) {

        #
        # fix for:
        # * set dtvmode
        # https://www.unitymediaforum.de/viewtopic.php?p=428995#p428995
        #
        for (i = 0; i < 20; ++i) {
            if (!ex("/opt/bin/mediaclient -d /dev/dvb/adapter" i "/frontend0 --setdtvmode=DVBC", 0, "^(Done)")) {
                break
            }
        }
        tuners_found = i
        if (!tuners_found) {
            PR("no Sundtek card(s) found, trying others (untested)")
            for (i = 0; i < 20; ++i) {
                if (ex("dvb-fe-tool -a " i " -d DVBC/ANNEX_A", 0, "(not found)")) {
                    break
                }
            }
            tuners_found = i
        }
        if (tuners_found >= tuners_cfg) {
            break
        } else if (!retry--) {
            exit_("retries exhausted, tuner count configured not satisfiable")
        } else {
            if (last != tuners_found) {
                # increment to give the remaining a chance
                last = tuners_found
                retry = RETRIES
            }
            PR("tuner count configured not yet reached [ " tuners_found " < " tuners_cfg " ], retrying [ " retry " ]")
        }
        ex("sleep 1")
    }
    PR("tuner(s) found: " tuners_found)
    for (i = 0; i < tuners_found; ++i) {
        ex("ln -nfs /bin/sh " INSTPATH "sh" i)
        SH[i] = INSTPATH "sh" i
    }
    while (1) {
        scan_down_channels()
        generate_rrdgraphs()
    }
}

func usage( \
    locked, line, a)
{
    PR("Usage: " PRGBNAME)
    while ((line = ex_line("cat " PRGNAME)) != -1) {
        if (locked) {
            if (match(line, "match[^,]+, \"(.+)\"[^\"]+# (.*)$", a)) {
                PR(sprintf("  %-21s - %s", gensub("\\\\$", "", "g", a[1]), a[2]))
            }
        } else {
            if (match(line, "^func process_cmdline")) ++locked
        }
    }
    PR("\nDefaults:")
    PR("  -c " sprintf("%-10d", RRDBASE_RRASTEPS) "   # 10 secs")
    PR("  -r " sprintf("%-10d", RRDBASE_HISTSIZE) "   # 1 week" )
    PR("  -w " sprintf("%-10d", RRDGRAPH_WIDTH)                 )
    PR("  -h " sprintf("%-10d", RRDGRAPH_HEIGHT)                )
    PR("\nExample 1 (continuously record new data):")
    PR(PRGBNAME " -d 1 -r 3600 -f 626:634 -i -n 3")
    PR(" - use display mode 1 (accumulated)")
    PR(" - keep 3600 secs of data")
    PR(" - scan 626 MHz and 634 MHz downstream channel frequencies")
    PR(" - ignore bit errors reported by DVB driver")
    PR(" - wait for at least 3 tuners to come up after start (useful after reboot)")
    PR(" - generate graphs of size " RRDGRAPH_WIDTH "x" RRDGRAPH_HEIGHT " (default)")
    PR(" - use all tuners found in parallel" " (default)")
    PR("\nExample 2 (take a single snapshot of existing data):")
    PR(PRGBNAME " -g 27500 1800 -w 1500 -h 800 -t /tmp")
    PR(" - extract a snapshot of 1800 secs length ending 27500 secs in the past")
    PR(" - generate graph of size 1500x800")
    PR(" - write graph to /tmp")
}

func process_cmdline( \
    rrd_exists, renew_rrd_base, backup_time, freq_list, i, a)
{
    read_cfg()
    if (FREQ_SPEC_CFG) freq_list = FREQ_SPEC_CFG
    if (RRA_STEPS_CFG) rrdbase_rrasteps = RRA_STEPS_CFG
    if (HIST_SIZE_CFG) rrdbase_histsize = HIST_SIZE_CFG

    #
    # regexps for arg matching are kept somewhat sloppy to avoid clutter in usage() text output
    #
    for (i = 2; i < _ARGC; ++i) {
        if (match(_ARGV[i] _ARGV[i + 1], "-h$")) { # print this help and exit
            usage()
            exit
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-f [0-9]+(:[0-9]+)*$")) { # specify downstream (DS) channel frequencies (in MHz)
            freq_list = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i], "-f$")) { # force scan of TC4400 for DS channel frequencies
            freq_list = "force_new_scan"
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-c [0-9]+$")) { # create a RRD base with given RRA steps (in secs)
            rrdbase_rrasteps = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-r [0-9]+$")) { # recording history size (in secs)
            rrdbase_histsize = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-t /[0-9a-zA-Z/_-]+$")) { # set destination directory for graph(s)
            target_dir = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-w [0-9]+$")) { # width of generated graph(s)
            rrdgraph_width = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-h [0-9]+$")) { # height of generated graph(s)
            rrdgraph_height = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1] " " _ARGV[i + 2], "-g [0-9]+ [0-9]+$")) { # generate a snapshot with stop at and length (in secs)
            rrdgraph_histend = strftime("%s") - _ARGV[i + 1]
            rrdgraph_length = _ARGV[i + 2]
            i += 2
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-d [01]$")) { # display mode (0 == standard, 1 == accumulated)
            display_mode = _ARGV[i + 1]
            ++i
        } else if (match(_ARGV[i] " " _ARGV[i + 1], "-n [0-9]+$")) { # wait for this many tuners minimum to come up
            tuners_cfg = _ARGV[i + 1]
            if (!tuners_cfg) {
                exit_("need more tuners")
            }
            ++i
        } else if (match(_ARGV[i], "-i$")) { # ignore errors reported by dvbtune
            ++dvbtune_ignerrs
        } else if (match(_ARGV[i], "-sequential$")) { # force sequential tuning in multi tuner mode
            ++sequential
        } else if (match(_ARGV[i], "-failreboot$")) { # trigger a reboot on dvbsnoop failure
            ++failreboot
        } else if (match(_ARGV[i], "-v$")) { # increase logging verbosity
            ++verbose
        } else {
            usage()
            ++ERR; exit
        }
    }
    retrieve_down_freqs(freq_list) 
    rrd_exists = ex("ls " RRDBASE_FILE, 0 ,"^(" RRDBASE_FILE ")$")
    if (!rrd_exists) {
        PR("the RRD does not exist")
        renew_rrd_base += 2
    }
    if (split(FREQ_SPEC_CFG, a, ":") != split(FREQ_SPEC_NEW, a, ":")) {
        if (FREQ_SPEC_CFG) PR("count of monitored channel frequencies did change")
        renew_rrd_base += 2
    } else if (FREQ_SPEC_CFG != FREQ_SPEC_NEW) {
        if (FREQ_SPEC_CFG) PR("values of monitored channel frequencies did change")
        renew_rrd_base += 1
    }
    if (RRA_STEPS_CFG != rrdbase_rrasteps \
     || HIST_SIZE_CFG != rrdbase_histsize) {
        if (RRA_STEPS_CFG) PR("RRD base recording parameters did change")
        renew_rrd_base += 2
    }
    if (rrd_exists && renew_rrd_base && rrdgraph_histend) {
        PR("can't extract graph: parameters currently in use don't match the data base")
        exit_("did you issue -g together with either -f, -c, -r ?")
    }
    if (target_dir) {
        target_dir = gensub("^(.*[^/])/*$", "\\\1/", "g", target_dir)
        if (ex("mkdir -p " target_dir, 2, "^(.*)$")) {
            exit_("can't create target directory")
        }
    } else {
        target_dir = INSTPATH
    }
    RRDGRAPH_NAME = target_dir "CableLoadMonitor"
    RRDGRAPH_TMP = RRDGRAPH_NAME RRDGRAPH_EXT "_"
    BWIDTH_SNAPSHOT = RRDGRAPH_NAME "_bwidth.txt"
    PR("graph display mode: " (display_mode ? "accumulated" : "standard"))
    PR("graph destination dir: " target_dir)
    if (rrdgraph_histend) {
        PR("point in time where the generated graph ends: " strftime("%y-%m-%d %T", rrdgraph_histend))
        PR("time span covered by the generated graph: " rrdgraph_length "s")
        PR("graph is written to: " RRDGRAPH_NAME RRDGRAPH_EXT)
        if (ex(assemble_rrdgraph_cmd(rrdgraph_histend, rrdgraph_length, "_"), verbose ? 0 : 256, "^(.*ERROR.*)$", a)) {
            PR(a[1])
            exit_("rrdtool graph fails")
        }
        ex("mv " RRDGRAPH_TMP "_" " " RRDGRAPH_NAME RRDGRAPH_EXT)
        exit
    }
    PR("recording RRA step size: " rrdbase_rrasteps " seconds")
    PR("recording RRA history size: "                                           \
        int(rrdbase_histsize                  / (60 * 60 * 24)) " day(s) "      \
        int(rrdbase_histsize % (60 * 60 * 24) / (60 * 60     )) " hour(s) "     \
        int(rrdbase_histsize % (60 * 60     ) / (60          )) " minute(s) "   \
        int(rrdbase_histsize % (60          )                 ) " second(s) "   \
    )
    PR("use multiple tuners: " (sequential ? "sequentially" : "parallel") " (if applicable)")
    if (dvbtune_ignerrs) PR("dvb bit errors reported: " "ignored")
    PR("dvb failure recovery method: " (failreboot ? "reboot" : "retry"))
    assemble_rrdcreate_cmd() 
    assemble_rrdgraphs_cmd()
    if (renew_rrd_base) {
        if (rrd_exists) {
            backup_time = strftime(".%Y-%m-%d_%T")
            PR("backing up old RRD data")
            ex("mv -v " RRDBASE_FILE " " RRDBASE_FILE backup_time, 2)
            ex("mv -v " CONF_FILE " " CONF_FILE backup_time, 2)
        }
        print FREQ_SPEC_NEW > CONF_FILE
        print RRDB_CCMD_NEW > CONF_FILE
        close(CONF_FILE)
        if (renew_rrd_base > 1) {
            PR("(re)creating the RRD base")
            if (ex(RRDB_CCMD_NEW, 2, "(.)")) {
                exit_("rrdtool create fails")
            }
        }
    }
    watch_the_scenery()
}

BEGIN {
    QUIET = 1
    setup()
    process_cmdline()
    exit
}

END { 
    cleanup()
    PRP("\n" PRGBNAME " " "exits with: " (ERR ? ERR : 0) " " "error(s)")
    PRP("====== [" sprintf(" %5d ", PROCINFO["pid"]) "] program stop  [" strftime() "] on " HOSTNAME " ======")
    close(PROTOCOL)
    exit ERR
}

!

[ -t 0 ] && STDIN="< /dev/null"
eval exec awk -f $0.awklib -f $0_$$.awk $STDIN 2>&1

